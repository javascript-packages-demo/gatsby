# [gatsby](https://www.npmjs.com/search?q=gatsby)

[**gatsby**](https://www.npmjs.com/search?q=gatsby)
![npm](https://img.shields.io/npm/dy/gatsby)
site generator for React. [gatsbyjs.com](https://www.gatsbyjs.com/)

 # Popularity
 * https://insights.stackoverflow.com/survey/2020#technology-web-frameworks
